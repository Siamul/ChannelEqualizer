/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package channelequalizer;

import java.util.Random;

/**
 *
 * @author Siamul Karim Khan
 */
public class RandomBitGenerator {
    public static int[] generateBitSequence(int length)
    {
        int[] retV = new int[length];
        Random r = new Random(System.currentTimeMillis());
        for(int i = 0; i<length; i++)
        {
            retV[i] = r.nextInt(2);
        }
        return retV;
    }
}
