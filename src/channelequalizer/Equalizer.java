/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package channelequalizer;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Siamul Karim Khan
 */
public class Equalizer {
    int[] dataBits;
    double[] dataValues;
    double[][] transitionProb;
    double[][] observationProb;
    int channelParameterSize;
    int vectorSize;
    String[] states;

    public Equalizer(int[] dataBits, double[] dataValues, int channelParameterSize, int vectorSize)
    {
        this.dataValues = dataValues;
        this.dataBits = dataBits;
        this.channelParameterSize = channelParameterSize;
        this.vectorSize = vectorSize;
        int numberStates = (int)Math.pow(2, channelParameterSize + vectorSize - 1);
        int numberBits = channelParameterSize + vectorSize - 1;
        states = new String[numberStates];
        for(int i = 0; i<states.length; i++)
        {
            String binaryString = Integer.toBinaryString(i);
            int zeropad = numberBits - binaryString.length();
            for(int j = 0; j<zeropad; j++)
            {
                binaryString = "0" + binaryString;
            }
            states[i] = binaryString;
        }
        transitionProb = new double[numberStates][numberStates];
        for(int i = 0; i<transitionProb.length; i++)
        {
            int j = Integer.parseInt(states[i].substring(1, states[i].length()) + "0", 2);
            int k = Integer.parseInt(states[i].substring(1, states[i].length()) + "1", 2);
            transitionProb[i][j] = 0.5;
            transitionProb[i][k] = 0.5;
        }
        observationProb = new double[numberStates][vectorSize];
        int[] count = new int[numberStates];
        for(int i = 0; i<dataBits.length - numberBits + 1; i++)
        {
            String str = new String();
            for(int k = 0; k<numberBits; k++)
            {
                str += Integer.toString(dataBits[i+k]);
            }
            int j = Integer.parseUnsignedInt(str, 2);
            for(int k = 0; k<vectorSize; k++)
            {
                observationProb[j][k] += dataValues[i+k];
            }
            count[j]++;
        }
        for(int i = 0; i < numberStates; i++)
        {
            for(int k = 0; k<vectorSize; k++)
            {
                observationProb[i][k] /= count[i];
            }
        }
    }
    int[] getBitSequence(double[] dataValues)
    {
        double[][] dMin = new double[dataValues.length - vectorSize + 1][states.length];
        double[] vector = new double[vectorSize];
        System.arraycopy(dataValues, 0, vector, 0, vectorSize);
        for(int i = 0; i<states.length; i++)
        {
            dMin[0][i] = distanceBetween(vector, observationProb[i]);
        }
        for(int i = 1; i<dataValues.length - vectorSize + 1; i++)
        {
            for(int j = 0; j<states.length; j++)
            {
                System.arraycopy(dataValues, i, vector, 0, vectorSize);
                int k1 = Integer.parseInt("0" + states[j].substring(0, states[j].length() - 1), 2);
                int k2 = Integer.parseInt("1" + states[j].substring(0, states[j].length() - 1), 2);
                dMin[i][j] = distanceBetween(vector, observationProb[j]) + Math.min(dMin[i-1][k1], dMin[i-1][k2]);
            }
        }
        int winstar = getMinIndex(dMin[dataValues.length - vectorSize]);
        String result = new String();
        result += states[winstar];
        for(int i = dataValues.length - vectorSize - 1; i>=0; i--)
        {
            int k1 = Integer.parseInt("0" + states[winstar].substring(0, states[winstar].length() - 1), 2);
            int k2 = Integer.parseInt("1" + states[winstar].substring(0, states[winstar].length() - 1), 2);
            //System.out.println(k1 + " " + k2);
            if(dMin[i][k1] < dMin[i][k2])
            {
                winstar = k1;
                result = "0" + result;
            }
            else
            {
                winstar = k2;
                result = "1" + result;
            }
        }
        String[] resArr = result.split("");
        int[] retV = new int[resArr.length];
        for(int i = 0; i<resArr.length; i++)
        {
            retV[i] = Integer.parseInt(resArr[i]);
        }
        return retV;
    }
    
    private int getMinIndex(double[] list)
    {
        int minIndex = -1;
        double min = Double.POSITIVE_INFINITY;
        for(int i = 0; i<list.length; i++)
        {
            if(list[i] < min)
            {
                min = list[i];
                minIndex = i;
            }
        }
        return minIndex;
    }
    
    private double distanceBetween(double[] point1, double[] point2)
    {
        double result = 0;
        for(int i = 0; i<point1.length; i++)
        {
            result += Math.pow((point1[i] - point2[i]),2);
        }
        return result;
    }
}
