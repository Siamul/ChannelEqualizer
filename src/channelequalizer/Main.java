/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package channelequalizer;

/**
 *
 * @author Siamul Karim Khan
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    static int trainBitsLength = 800;
    static int testBitsLength = 200;
    public static void main(String[] args) {
        // TODO code application logic here
        int[] trainBits = RandomBitGenerator.generateBitSequence(trainBitsLength);
        int[] testBits = RandomBitGenerator.generateBitSequence(testBitsLength);
        double[] parameters = {0.5, 1};
        Channel channel = new Channel(parameters, 0, 0.5);
        double[] trainChanSeq = channel.valuesFromChannel(trainBits);
        double[] testChanSeq = channel.valuesFromChannel(testBits);
        Equalizer equalize = new Equalizer(trainBits, trainChanSeq, parameters.length, 2);
        int[] bitSeq = equalize.getBitSequence(testChanSeq);
        System.out.print("Actual:    ");
        for(int i = 0; i<testBits.length; i++)
        {
            System.out.print(testBits[i] + " ");
        }
        System.out.println();
        System.out.print("Equalized: ");
        for(int i = 0; i<bitSeq.length; i++)
        {
            System.out.print(bitSeq[i] + " ");
        }
        System.out.println();
        int correct = 0;
        for(int i = 0; i<testBits.length; i++)
        {
            if(testBits[i] == bitSeq[i]) correct++;
        }
        double accuracy = 0;
        for(int i = 0; i<100; i++)
        {
            accuracy += (double)correct/(double)testBits.length;
        }
        System.out.println("Accuracy = " + accuracy);
    }
    
}
