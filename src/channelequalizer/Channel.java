/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package channelequalizer;

import java.util.Random;

/**
 *
 * @author Siamul Karim Khan
 */
public class Channel {
    private Random r;
    private double[] parameters;
    private double noiseMean;
    private double noiseStdDev;
    public Channel(double[] parameters, double noiseMean, double noiseStdDev)
    {
        r = new Random(System.currentTimeMillis());
        this.parameters = new double[parameters.length];
        System.arraycopy(parameters, 0, this.parameters, 0, parameters.length);
        this.noiseMean = noiseMean;
        this.noiseStdDev = noiseStdDev;
    }
    private double getNoise()
    {
        double stdGaussianVal = r.nextGaussian();
        return noiseMean + noiseStdDev * stdGaussianVal;
    }
    public void setParameters(double[] parameters)
    {
        this.parameters = parameters;
    }
    public double[] getParameters()
    {
        return parameters;
    }

    public void setNoiseMean(double noiseMean) {
        this.noiseMean = noiseMean;
    }

    public void setNoiseStdDev(double noiseStdDev) {
        this.noiseStdDev = noiseStdDev;
    }

    public double getNoiseMean() {
        return noiseMean;
    }

    public double getNoiseStdDev() {
        return noiseStdDev;
    }
    public double ChannelAction(int[] I)
    {
        double cleanResult = 0;
        for(int i = 0; i<parameters.length; i++)
        {
            cleanResult += parameters[i] * (double)I[i];
        }
        return cleanResult + getNoise();
    }
    public double[] valuesFromChannel(int[] bits)
    {
        double[] channelSignals = new double[bits.length - parameters.length + 1];
        int[] temp = new int[parameters.length];
        for(int i = 0; i < channelSignals.length; i++)
        {
            System.arraycopy(bits, i, temp, 0, parameters.length);
            channelSignals[i] = ChannelAction(temp);
        }
        return channelSignals;
    }
}
